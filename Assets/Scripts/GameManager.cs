using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 게임 전반적인 관리를 담당하는 GameManager 클래스
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// 플레이어 캐릭터를 가져오는 프로퍼티
    /// </summary>
    public PlayerCharacter player => Player;

    /// <summary>
    /// 플레이어 캐릭터를 설정하는 메서드
    /// </summary>
    /// <param name="player"></param>
    public void SetPlayer(PlayerCharacter player) { Player = player; }

    /// <summary>
    /// GameManager의 인스턴스를 가져오는 프로퍼티
    /// </summary>
    public static GameManager instance => m_Instance ??  (m_Instance = CreateInstance());

    /// <summary>
    /// UI GameObject를 담을 변수
    /// </summary>
    public GameObject UI;

    /// <summary>
    /// GameManager의 인스턴스를 담을 변수
    /// </summary>
    private static GameManager m_Instance;

    /// <summary>
    /// 플레이어 캐릭터를 담을 변수
    /// </summary>
    private PlayerCharacter Player;

    /// <summary>
    /// 생성자를 private으로 선언하여 외부에서 인스턴스화 방지
    /// </summary>
    private GameManager() { }

    /// <summary>
    /// GameManager 인스턴스 생성 메서드
    /// </summary>
    /// <returns>생성된 GameManager의 인스턴스</returns>
    private static GameManager CreateInstance()
    {
        // GameManager의 인스턴스가 없는 경우에만 실행
        if (m_Instance == null)
        {
            // 새로운 GameObject를 생성하여 GameManager 컴포넌트를 추가
            GameObject newGameManager = new GameObject("GameManager");
            m_Instance = newGameManager.AddComponent<GameManager>();

            // 씬 전환 시에도 유지되도록 설정
            DontDestroyOnLoad(newGameManager);
        }

        // 생성된 GameManager의 인스턴스 반환
        return m_Instance;
    }

    private void Awake()
    {
        // Awake 함수가 호출될 때 인스턴스를 설정
        m_Instance = this;
    }


    /// <summary>
    /// 게임 오버시 호출되는 메소드
    /// </summary>
    public void GameOver()
    {
        // UI 활성화
        UI.SetActive(true);

        // 플레이어를 비활성화
        player.gameObject.SetActive(false);
    }

    /// <summary>
    /// 게임 재시작시 호출되는 메소드
    /// </summary>
    public void GameReStart()
    {
        //GameScene을 다시 로드
        SceneManager.LoadScene("GameScene");
    }

}
