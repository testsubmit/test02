using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    /// <summary>
    /// 입력 이벤트를 처리할 대리자 선언
    /// </summary>
    /// <param name="axis"></param>
    public delegate void InputEventListener(Vector2 axis);

    /// <summary>
    /// 입력 이벤트 선언
    /// </summary>
    private InputEventListener inputEventListener;

    /// <summary>
    /// 입력 이벤트 리스너를 추가하는 메소드
    /// </summary>
    /// <param name="listener"></param>
    public void AddInputEventListener(InputEventListener listener)
        => inputEventListener += listener;



    /// <summary>
    /// InputSystem의 Move 액션에 대한 이벤트 핸들러
    /// </summary>
    /// <param name="axis"></param>
    private void OnMove(InputValue axis)
    {
        // 입력값을 벡터로 가져옴
        Vector2 dir = axis.Get<Vector2>();

        // 입력값이 일정 값 미만일 경우 처리하지 않음
        if (dir.magnitude < 0.1f) return;

        // 등록된 입력 이벤트 리스너 호출
        inputEventListener?.Invoke(dir);
    }

}
