using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MainSceneUIManager : MonoBehaviour
{
    /// <summary>
    /// 메인씬으로 이동하는 버튼
    /// </summary>
    public Button mainButton;

    /// <summary>
    /// 재시작 버튼
    /// </summary>
    public Button restartButton;

    void Start()
    {
        //메인씬으로
        mainButton.onClick.AddListener(OnGotoMainSceneButtonClicked);

        //게임 재시작
        restartButton.onClick.AddListener(onGameRestartButtonClicked);
    }

    /// <summary>
    /// 게임 재시작
    /// </summary>
    private void onGameRestartButtonClicked()
    {
        GameManager gameManager = GameManager.instance;
        gameManager.GameReStart();
    }

    /// <summary>
    /// 메인 씬으로 가는 메소드
    /// </summary>
    private void OnGotoMainSceneButtonClicked()
    {
        // Main씬을 로드
        SceneManager.LoadScene("MainScene");
    }

}
