using UnityEngine;
using UnityEngine.InputSystem;


public class MoveToTargetObject : MonoBehaviour
{
    /// <summary>
    /// 이동 속도
    /// </summary>
    public float moveSpeed;

    /// <summary>
    /// 현재 이동하고 있는지를 나타냄
    /// </summary>
    public bool isMove { get; private set; }

    /// <summary>
    /// 이동 목표지점
    /// </summary>
    private Vector3 TargetPos;


    /// <summary>
    /// 이동방향을 반환합니다.
    /// </summary>
    private Vector3 MoveDir => (TargetPos - transform.position).normalized;


    /// <summary>
    /// 이동한다고 가정했을떄, 다음 프레임의 위치를 반환
    /// </summary>
    private Vector3 nextPos => transform.position + MoveDir * moveSpeed * Time.fixedDeltaTime;


    /// <summary>
    /// 현재 도착상태인지를 판별
    /// </summary>
    private bool isArrived
        => Vector3.Distance(transform.position, TargetPos) < 0.1f ||
        Vector3.Distance(transform.position, nextPos) > Vector3.Distance(transform.position, TargetPos);



    private void Start()
    {
        moveSpeed = PlayerCharacter.PLAYER_MOVE_SPEED;
    }


    private void FixedUpdate()
    {
        if (isMove)
        {
            if (isArrived)
            {
                SetArrive();
            }
            else
            {
                transform.position = nextPos;
            }

        }
    }

    /// <summary>
    /// 목표지점을 설정하고, 움직임을 시작
    /// </summary>
    /// <param name="target"> 설정할 목표지점 </param>
    protected virtual void SetTargetPos(Vector3 target)
    {
        TargetPos = target;
        isMove = true;
    }


    protected virtual void SetArrive()
    {
        transform.position = TargetPos;
        isMove = false;
    }

}

