using UnityEngine;

/// <summary>
/// 충돌 결과를 정의하는 열거형
/// </summary>
public enum CollisionList
{
    NONE,
    STATIC,
    Boat
}

/// <summary>
/// 플레이어 이동 방향을 확인하는 클래스
/// </summary>
public class PlayerCheckDirection : MonoBehaviour
{
     /// <summary>
    /// 플레이어를 탑승하는 클래스에 대한 참조
    /// </summary>
    public PlayerRide playerRide;

    private BoxCollider _BoxCollider;

    /// <summary>
    /// 플레이어의 상자 충돌체
    /// </summary>
    private BoxCollider boxCollider => _BoxCollider ??
        (_BoxCollider = GetComponent<BoxCollider>());

    /// <summary>
    /// 충돌을 확인할 레이어 마스크
    /// </summary>
    public LayerMask checkLayer;

    /// <summary>
    /// 충돌을 확인하고 그 결과를 반환
    /// </summary>
    /// <param name="start">시작 지점</param>
    /// <param name="dir">방향 벡터</param>
    /// <param name="hitResult">충돌 결과를 저장할 변수</param>
    /// <returns>충돌 결과를 나타내는 열거형 값</returns>
    public CollisionList CheckCollision(Vector3 start, Vector3 dir, out RaycastHit hitResult)
    {
        RaycastHit hitInfo;

        // 시작 위치를 조정
        start += new Vector3(0, 0.5f, 0);

        // 레이캐스트를 사용하여 충돌을 확인
        bool isHit = Physics.Raycast(start, dir, out hitInfo, 2, checkLayer);

        // 충돌 결과를 저장
        hitResult = hitInfo;

        // 충돌이 없는 경우 NONE을 반환
        if (!isHit)
        {
            return CollisionList.NONE;
        }

        // 충돌한 객체의 레이어에 따라 다른 결과를 반환
        else
        {
            if (hitInfo.transform.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
            {
                return CollisionList.STATIC;
            }
            else if (hitInfo.transform.gameObject.layer == LayerMask.NameToLayer("Boat"))
            {
                return CollisionList.Boat;
            }

            else return CollisionList.NONE;
        }
    }
}

