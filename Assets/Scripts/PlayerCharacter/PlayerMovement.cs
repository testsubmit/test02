using UnityEngine;
using UnityEngine.UIElements;


/// <summary>
/// 플레이어 이동을 담당하는 클래스.
/// </summary>
public class PlayerMovement : MoveToTargetObject
{
    /// <summary>
    /// 플레이어 입력 매니저
    /// </summary>
    public PlayerInputManager playerInputManager;

    /// <summary>
    /// 플레이어의 이동 가능 여부를 나타내는 변수
    /// </summary>
    private PlayerMovable PlayerCanMove;

    /// <summary>
    /// 시작 위치
    /// </summary>
    Vector3 startPos;

    /// <summary>
    /// 이동 방향
    /// </summary>
    Vector3 direction;

    /// <summary>
    /// Start 함수에서 호출되며, 수평 이동 입력 이벤트 리스너를 추가
    /// </summary>
    private void Start()
    {
        // 수평 이동 이벤트 리스너 추가
        playerInputManager.horizontal += MoveHorizontal;

        // 수평 이동 타겟 설정
        playerInputManager.horizontalMoveToTarget = this;
    }

    /// <summary>
    /// 수평 이동 메서드
    /// </summary>
    /// <param name="dir"></param>
    private void MoveHorizontal(int dir)
    {
        if (base.isMove) return;

        dir *= 2;

        base.SetTargetPos(
            transform.position + new Vector3(dir, 0, 0));
    }

    /// <summary>
    /// 타겟 위치를 설정하는 메서드
    /// </summary>
    /// <param name="target"></param>
    protected override void SetTargetPos(Vector3 target)
    {
        base.SetTargetPos(target);

    }

    /// <summary>
    /// 바닥 체크 메서드
    /// </summary>
    public void GroundCheck()
    {
        // 시작 위치와 이동 방향 설정
        startPos = transform.position + new Vector3(0, 1, 0);
        direction = Vector3.down;

        // 레이캐스트를 통해 바닥을 체크
        if (Physics.Raycast(startPos, direction, out RaycastHit hitInfo, 5, ~LayerMask.NameToLayer("Player")))
        {
            print($"Hit: {hitInfo.transform.gameObject.layer}");
        }
        else
        {
            print("NMone");
        }
    }
}
