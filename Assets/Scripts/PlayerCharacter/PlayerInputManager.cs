using UnityEngine;
using UnityEngine.Windows;

/// <summary>
/// 플레이어 입력 매니저
/// </summary>
public class PlayerInputManager : MonoBehaviour
{
    /// <summary>
    /// 플레이어 이동 방향을 확인하는 컴포넌트
    /// </summary>
    public PlayerCheckDirection checkDirection;

    /// <summary>
    /// 플레이어가 탈 수 있는 오브젝트 컴포넌트
    /// </summary>
    public PlayerRide playerCanRide;

    /// <summary>
    /// 플레이어 회전을 관리하는 컴포넌트
    /// </summary>
    public PlayerRotationManager playerRotationManager;

    /// <summary>
    /// 입력 관리자 선언
    /// </summary>
    public InputManager inputManager;

    public delegate void SetPlayerMoveDirection(int dir);

    /// <summary>
    /// 수평 이동 방향 설정 대리자
    /// </summary>
    public SetPlayerMoveDirection horizontal;

    /// <summary>
    /// 수직 이동 방향 설정 대리자
    /// </summary>
    public SetPlayerMoveDirection vertical;

    /// <summary>
    /// 수평 이동 타겟 오브젝트
    /// </summary>
    public MoveToTargetObject horizontalMoveToTarget;

    /// <summary>
    /// 수직 이동 타겟 오브젝트
    /// </summary>
    public MoveToTargetObject verticalMoveToTarget;


    private void Start()
    {
        //Start 함수에서 호출되며, 입력 이벤트 리스너를 추가
        inputManager.AddInputEventListener(GetInput);
    }

    /// <summary>
    /// 입력이 가능한지 여부를 확인하는 메소드
    /// </summary>
    /// <returns></returns>
    private bool CanInputMove()
    {
        bool ret = false;

        ret = !horizontalMoveToTarget.isMove && !verticalMoveToTarget.isMove;

        return ret;
    }

    /// <summary>
    /// 오른쪽 X 값으로 설정하는 메소드
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    private float SetRightXValue(float x)
    {
        float ret = x;

        float diff = (x % 2);

        if (diff > 1)
            ret += (2 - diff);
        else
            ret -= diff;

        return ret;
    }

    /// <summary>
    /// 입력을 받는 메소드
    /// </summary>
    /// <param name="input"></param>
    private void GetInput(Vector2 input)
    {
        // 입력된 벡터를 방향 벡터로 변환
        Vector3 dir = ConvertDirection(input);

        // 플레이어의 현재 위치를 가져와서 X 좌표를 정렬
        Vector3 startPos = transform.position;
        startPos.x = SetRightXValue(startPos.x);
        
        // 충돌 여부와 정보를 확인
        CollisionList collisionResult = checkDirection.CheckCollision(startPos , dir, out RaycastHit hitResult);

        // 충돌이 없는 경우
        if (collisionResult == CollisionList.NONE)
        {
            // 이동이 가능한지 확인하고 불가능한 경우 이후 코드를 실행하지 않음
            if (!CanInputMove()) return;

            // 플레이어 위치를 정렬된 X 좌표로 설정
            Vector3 setPos = transform.position;
            setPos.x = SetRightXValue(setPos.x);

            transform.position = setPos;


            // 플레이어가 탑승 중인 경우 탑승 취소
            if (playerCanRide.isRide)
            {
                playerCanRide.ClearRideTransform();
            }
            // 수직 이동인 경우 수직 이동 이벤트 발생, 아닌 경우 수평 이동 이벤트 발생
            if (Mathf.Abs(dir.z) > 0.1) vertical?.Invoke((int)dir.z);
            else horizontal?.Invoke((int)dir.x);
        }

        // 보트와 충돌한 경우
        else if (collisionResult == CollisionList.Boat)
        {
            // 플레이어가 탑승할 수 있는 오브젝트로 설정
            playerCanRide.SetRideTransform(hitResult.transform);

            // 수직 이동인 경우 수직 이동 이벤트 발생
            if (Mathf.Abs(dir.z) > 0.1) vertical?.Invoke((int)dir.z);
        }

        // 플레이어의 회전 설정
        SetPlayerRotation(dir);
    }

    /// <summary>
    /// 플레이어의 회전을 설정하는 메소드
    /// </summary>
    /// <param name="moveDir"></param>
    private void SetPlayerRotation(Vector3 moveDir)
    {
        playerRotationManager.SetPlayerRot(moveDir);
    }

    /// <summary>
    /// 입력된 방향을 변환하는 메소드
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    private Vector3 ConvertDirection(Vector2 dir)
    {
        // 수직 입력이 있는지 확인하여 방향 벡터 생성
        Vector3 ret = Mathf.Abs(dir.y) > 0.1f ? new Vector3(0, 0, dir.y) : new Vector3(dir.x, 0, 0);

        // 방향 벡터를 정규화하여 반환
        ret.Normalize();
        return ret;
    }

}
