using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 탑승을 관리하는 클래스.
/// </summary>
public class PlayerRide : MonoBehaviour
{
    /// <summary>
    /// 탑승 속도
    /// </summary>
    public float rideSpeed = 20;

    /// <summary>
    /// 탑승 트랜스폼을 설정하는 메소드
    /// </summary>
    /// <param name="rideTr"></param>
    public void SetRideTransform(Transform rideTr)
    {
        // 부모를 탑승 트랜스폼으로 설정하고, 지정된 위치로 이동
        transform.parent = rideTr;
        transform.localPosition = new Vector3(0, -.5f, 0);
    }

    /// <summary>
    /// 탑승 트랜스폼을 해제하는 메소드
    /// </summary>
    public void ClearRideTransform() 
    { 
        transform.parent = null;
    }
 
    /// <summary>
    /// 플레이어가 탑승 중인지 여부를 반환하는 프로퍼티
    /// </summary>
    public bool isRide => transform.parent != null;    
}
