using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 플레이어의 이동 여부를 제어
/// </summary>
public class PlayerMovable : MonoBehaviour
{
    public LayerMask checkLayer;
   
    private BoxCollider BoxCollider;

    /// <summary>
    /// 플레이어의 상자 충돌체
    /// </summary>
    private BoxCollider boxCollider => BoxCollider ?? (BoxCollider = GetComponent<BoxCollider>());

    /// <summary>
    /// 지정된 방향으로 이동할 수 있는지 여부를 반환
    /// </summary>
    /// <param name="dir">이동 방향.</param>
    /// <returns>이동 가능한 경우 true를 반환하고, 그렇지 않으면 false를 반환.</returns>
    public bool canMove(Vector3 dir)
        => Physics.BoxCast(transform.position, boxCollider.size * 0.5f, dir, Quaternion.identity, boxCollider.size.magnitude, LayerMask.GetMask("Obstacle"));

}
