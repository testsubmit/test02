using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    /// <summary>
    /// 플레이어의 이동 속도를 나타내는 정적 변수
    /// </summary>
    public static float PLAYER_MOVE_SPEED = 10;

    /// <summary>
    /// 객체가 활성화될 때 호출되는 메소드.
    /// </summary>
    private void Start()
    {
        //GameManager의 인스턴스에 플레이어를 설정
        GameManager.instance.SetPlayer(this);
    }

    /// <summary>
    /// 플레이어가 충돌했을 때 호출되는 메소드 
    /// 게임 매니저의 게임 오버 함수를 호출
    /// </summary>
    public void PlayerGameOver()
    {
        GameManager gameManager = GameManager.instance;
        gameManager.GameOver();
    }
}
