using UnityEngine;

/// <summary>
/// 플레이어의 회전을 관리하는 클래스
/// </summary>
public class PlayerRotationManager : MonoBehaviour
{
    /// <summary>
    /// // 플레이어의 현재 회전 각도를 저장
    /// </summary>
    private float PlayerRotation;

    /// <summary>
    /// 회전 속도를 조절하는 변수
    /// </summary>
    private float RotationSpeed = 0.05f;

    /// <summary>
    ///  회전을 부드럽게 만듦
    /// </summary>
    private float TurnVelocity;

    /// <summary>
    /// 플레이어의 회전 각도를 설정하는 메소드
    /// </summary>
    private void Start()
    {
        SetPlayerRot(transform.eulerAngles.y);
    }

    /// <summary>
    /// 이동 방향을 기준으로 플레이어의 회전 각도를 설정하는 메소드
    /// </summary>
    /// <param name="moveDirection">이동 방향</param>
    public void SetPlayerRot(Vector3 moveDirection)
    {
        // 이동 방향을 각도로 변환하여 플레이어의 회전 각도를 설정
        float angle = Mathf.Atan2(moveDirection.x, moveDirection.z) * Mathf.Rad2Deg;
        SetPlayerRot(angle);
    }

    /// <summary>
    /// 플레이어의 회전 각도를 설정하는 메소드
    /// </summary>
    /// <param name="rot">플레이어의 회전 각도</param>
    public void SetPlayerRot(float rot)
    {
        PlayerRotation = rot;    
    }


    private void FixedUpdate()
    {
        // 현재 플레이어의 각도를 가져옴
        float currentAngle = transform.eulerAngles.y;

        // 부드러운 회전 각도를 계산
        float smoothAngle = Mathf.SmoothDampAngle(currentAngle, PlayerRotation, ref TurnVelocity, RotationSpeed);

        // 부드러운 회전 각도로 플레이어를 회전
        transform.rotation = Quaternion.Euler(0, smoothAngle, 0);
    }


}

