using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

/// <summary>
/// 플레이어의 애니메이션을 제어하는 PlayerAnimController 클래스
/// </summary>
public class PlayerAnimController : MonoBehaviour
{
    /// <summary>
    ///  Animator 컴포넌트에 대한 참조를 저장
    /// </summary>
    private Animator Animation;

    /// <summary>
    /// Animator 컴포넌트에 대한 참조를 반환
    /// </summary>
    /// <returns>Animator 컴포넌트</returns>
    private Animator anim => Animation ?? (Animation = GetComponent<Animator>());    
}
