using System;
using UnityEngine;


namespace LineObject
{
    /// <summary>
    /// 잔디의 선 객체를 나타내는 클래스.
    /// </summary>
    public class LineObject_Grass : StaticRoadSample
    {
        // 나무들을 나타내는 배열
        public GameObject[] tree;

        // 위치의 최소 및 최대값
        public int minPosition, maxPosition;

        // 생성할 나무의 최소 및 최대 개수
        public int minNum, maxNum;

        private void Awake()
        {
            // 잔디 초기화 메서드 호출
            base.Initialized(tree, minPosition, maxPosition, minNum, maxNum);
        }
    }
}

