using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 강 리버 타일의 콜라이더
/// </summary>
public class RiverColliderController : MonoBehaviour
{
    /// <summary>
    /// 강 타일의 콜라이더 생성
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        GameManager.instance.GameOver();
    }

}
