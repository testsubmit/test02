using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LineObject
{
    /// <summary>
    /// 도로의 선 객체를 나타내는 클래스.
    /// </summary>
    public class LineObject_Road : DynamicLineSample
    {
        /// <summary>
        /// 차량들을 나타내는 배열
        /// </summary>
        public GameObject[] car;

        /// <summary>
        /// 생성 위치를 나타내는 트랜스폼
        /// </summary>
        public Transform createTransform;

        /// <summary>
        /// 종료 위치를 나타내는 트랜스폼
        /// </summary>
        public Transform endTransform;

        /// <summary>
        /// 생성 간격의 최소 및 최대값
        /// </summary>
        public float createMinTerm, createMaxTerm;
        
        private void Awake()
        {
            // 도로 초기화 메서드 호출
            base.Initailized(car, createTransform, endTransform, createMinTerm, createMaxTerm);
        }   
    }
}

