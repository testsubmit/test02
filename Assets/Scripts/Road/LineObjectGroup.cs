using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 이동 대상 오브젝트를 특정 방향으로 이동시키는 클래스
/// </summary>
public class LineObjectGroup : MoveToTargetObject
{
    /// <summary>
    /// 이동 방향을 설정하는 메서드입니다.
    /// </summary>
    /// <param name="dir">이동할 방향 벡터</param>
    public void SetMoveDir(Vector3 dir)
    {
        // 현재 위치에서 지정된 방향으로 이동할 위치를 계산하여 설정
        base.SetTargetPos(transform.position + dir);
    }
}
