using UnityEngine;

namespace LineObject
{
    /// <summary>
    /// LineObject_Dirt의 선 객체를 나타내는 클래스
    /// </summary>
    public class LineObject_Dirt : StaticRoadSample
    {
        /// <summary>
        /// 바위들을 나타내는 배열
        /// </summary>
        public GameObject[] rock;

        /// <summary>
        /// 위치의 최소 및 최대값
        /// </summary>
        public int minPositon, maxPosition;

        /// <summary>
        /// 생성할 바위의 최대 및 최소 갯수
        /// </summary>
        public int minNum, maxNum;

        private void Awake()
        {
            base.Initialized(rock, minPositon, maxPosition, minNum, maxNum);
        }
    }
}

