using MoveObejct;
using System.Collections;
using UnityEngine;

namespace LineObject
{
    /// <summary>
    /// LineObject 클래스를 상속하는 DynamicLineSample 추상 클래스
    /// </summary>
    public abstract class DynamicLineSample : LineAbstractClass
    {

        // 생성 가능 여부 및 생성 쿨타임을 관리하는 변수와 코루틴
        private bool canCreate = true;

        private Coroutine coolTimeCo;

        /// <summary>
        /// 이동 가능한 오브젝트 배열
        /// </summary>
        protected GameObject[] m_MovableObject { get; private set; }

        /// <summary>
        /// 무작위 이동 가능한 오브젝트 반환
        /// </summary>
        private GameObject getRandomMovableObject => m_MovableObject[Random.Range(0, m_MovableObject.Length)];

        /// <summary>
        /// 동적 오브젝트 생성 위치
        /// </summary>
        private Transform createPosition;

        /// <summary>
        /// 동적 오브젝트 삭제 위치
        /// </summary>
        private Transform deletePosition;

        /// <summary>
        /// 동적 오브젝트 생성 간격의 최소 시간
        /// </summary>
        private float createMinTerm;

        /// <summary>
        /// 동적 오브젝트 생성 간격의 최대 시간
        /// </summary>
        private float createMaxTerm;

        /// <summary>
        /// 무작위 생성 간격을 가져오는 프로퍼티
        /// </summary>
        private float getRandomTerm => Random.Range(createMinTerm, createMaxTerm);


        private void Update()
        {
            if (canCreate)
            {
                CreatMovableObject();
            }
        }

        /// <summary>
        /// 동적 도로 샘플 초기화 메소드
        /// </summary>
        /// <param name="movableObject">이동 가능한 오브젝트 배열</param>
        /// <param name="createPosition">이동 가능한 오브젝트가 생성될 위치</param>
        /// <param name="endPosition">이동 가능한 오브젝트의 종료 위치</param>
        /// <param name="minTerm">이동 가능한 오브젝트가 생성되는 최소 간격</param>
        /// <param name="maxTerm">이동 가능한 오브젝트가 생성되는 최대 간격</param>
        protected void Initailized(GameObject[] movableObject, Transform createPosition, Transform endPosition, float minTerm, float maxTerm)
        {
            this.m_MovableObject = movableObject;
            this.createPosition = createPosition;
            this.deletePosition = endPosition;
            this.createMinTerm = minTerm;
            this.createMaxTerm = maxTerm;
        }

        /// <summary>
        /// 이동 가능한 오브젝트 생성
        /// </summary>
        private void CreatMovableObject()
        {
           // 오브젝트 배열이나 생성 위치가 없으면 생성하지 않음
            if (m_MovableObject == null) return;

            // 무작위 이동 가능한 오브젝트를 생성 위치에 인스턴스화
            if (createPosition == null) return;

            // 이동 가능한 오브젝트 초기화 및 부모 설정
            GameObject movableOb =
            Instantiate(getRandomMovableObject, createPosition.position, createPosition.rotation);

            // 생성 쿨타임 설정
            movableOb.GetComponent<MoveObject>().Initialized(createPosition, deletePosition);
            movableOb.transform.parent = transform;

            // 생성 쿨타임을 설정하고 생성 가능 여부를 false로 변경
            canCreate = false;
            coolTimeCo = StartCoroutine(CreateDelay(getRandomTerm));

        }

        /// <summary>
        /// 생성 쿨타임을 적용하는 코루틴
        /// </summary>
        /// <param name="second">쿨타임 시간(초)</param>
        private IEnumerator CreateDelay(float second)
        {
            // 주어진 시간만큼 대기한 후 생성 가능 여부를 true로 변경합니다.
            yield return new WaitForSeconds(second);
            canCreate = true;
        }
    }
}



