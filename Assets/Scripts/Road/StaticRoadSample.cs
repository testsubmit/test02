﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

// <summary>
/// 도로 샘플의 추상 기본 클래스
/// </summary>
namespace LineObject
{
    public abstract class StaticRoadSample : LineAbstractClass
    {
        /// <summary>
        /// 위치 단위
        /// </summary>
        public const int posUnit = 2;

        /// <summary>
        /// 길에 배치될 오브젝트
        /// </summary>
        protected GameObject[] LandScaping { get; private set; }

        /// <summary>
        /// 사용된 위치 목록
        /// </summary>
        private List<int> usedPositions = new List<int>();

        /// <summary>
        /// 사용된 최소 위치
        /// </summary>
        private int usedMinPos;

        /// <summary>
        /// 사용된 최대 위치
        /// </summary>
        private int usedMaxPos;

        /// <summary>
        /// 위치가 비어 있는지 확인하는 메소드
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        private bool isEmptyPos(int pos) => !usedPositions.Contains(pos);

        /// <summary>
        /// 랜드스케이핑을 설정하는 메소드
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public bool SetLandScaping(int pos)
        {
            // 이미 사용된 위치인 경우 반환
            if (!isEmptyPos(pos))       return false;

            // 랜드스케이핑이 비어 있는 경우 반환
            if (LandScaping == null)  return false;

            // 랜드스케이핑 오브젝트를 생성하고 설정합니다.
            GameObject landScapingObject = Instantiate(LandScaping[UnityEngine.Random.Range(0, LandScaping.Length)]);
            landScapingObject.transform.parent = transform;
            landScapingObject.transform.localPosition = new Vector3(pos, 1, 0);

            // 사용된 위치 목록에 추가
            usedPositions.Add(pos);

            return true;
        }

        /// <summary>
        /// 랜덤하게 빈 위치를 가져오는 메소드
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        private List<int> GetRandomEmptyPosition(int range)
        {
            List<int> ret = new List<int>();

            // 사용된 최소 위치부터 최대 위치까지 반복하면서 빈 위치를 찾음
            List<int> emptys = new List<int>();
            for (int i = usedMinPos; i <= usedMaxPos; i += posUnit)
            {
                if (isEmptyPos(i)) emptys.Add(i);
            }

            // 랜덤하게 빈 위치를 선택하여 반환 리스트에 추가
            for (int i = 0; i < range; ++i)
            {
                int index = UnityEngine.Random.Range(0, emptys.Count);

                ret.Add(emptys[index]);
                emptys.RemoveAt(index);
            }           

            return ret;
        }

        /// <summary>
        /// 초기화 메소드
        /// </summary>
        /// <param name="landScaping">길에 배치될 랜드스케이핑 오브젝트 배열</param>
        /// <param name="minPos">배치할 위치의 최소값</param>
        /// <param name="maxPos">배치할 위치의 최대값</param>
        /// <param name="minNum">랜드스케이핑을 배치할 최소 개수</param>
        /// <param name="maxNum">랜드스케이핑을 배치할 최대 개수</param>
        protected void Initialized(GameObject[] landScaping, int minPos, int maxPos, int minNum, int maxNum)
        {
            // 길에 배치될 랜드스케이핑 오브젝트 배열을 설정합니다.
            this.LandScaping = landScaping;

            this.usedMinPos = minPos;
            this.usedMaxPos = maxPos;

            // 랜덤하게 빈 위치를 찾아 랜드스케이핑을 배치합니다.
            int num = UnityEngine.Random.Range(minNum, maxNum);

            foreach(int pos in GetRandomEmptyPosition(num))
            {
                SetLandScaping(pos);
            }
        }
    }
}
