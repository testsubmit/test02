using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LineObject
{
    public class LineObject_River : DynamicLineSample
    {
        /// <summary>
        ///보트에 대한 게임 오브젝트 배열
        /// </summary>
        public GameObject[] Boat;


        /// <summary>
        /// 보트 생성 간격의 최소 시간
        /// </summary>
        public float createMinTerm;

        /// <summary>
        /// 보트 생성 간격의 최대 시간
        /// </summary>
        public float createMaxTerm;

        /// <summary>
        /// 보트 생성 위치
        /// </summary>
        public Transform createPosition;

        /// <summary>
        /// 보트 삭제 위치
        /// </summary>
        public Transform deletePosition;

        private void Awake()
        {
            // DynamicLineSample 클래스의 초기화 메서드를 호출하여 설정값 전달
            base.Initailized(Boat, createPosition, deletePosition, createMinTerm, createMaxTerm);
        }
    }
}

