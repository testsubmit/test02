using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// 메인씬에서 버튼별로 씬 이동을 하는 코드
/// </summary>
public class MainScene : MonoBehaviour
{    
    /// <summary>
    /// 게임씬으로 이동 버튼
    /// </summary>
    public Button gotoGainSceneButton;

    private void Awake()
    {
        //메인씬 이동 버튼
        gotoGainSceneButton.onClick.AddListener(OnGotoMainScene);
    }    

    /// <summary>
    /// 메인 씬 재로드 메소드
    /// </summary>
    private void OnGotoMainScene()
    {
        // MainScene 로드
        SceneManager.LoadScene("GameScene");
    }
}
