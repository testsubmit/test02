using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /// <summary>
    /// 플레이어와 카메라의 기본 상대 위치 및 회전
    /// </summary>
    private readonly Vector3 DEFAULT_LOCAL_POSITION = new Vector3(0, 10, -5);
    private readonly Vector3 DEFAULT_LOCAL_ROTATION = new Vector3(50, 0, 0);

    /// <summary>
    /// 플레이어 위치에 대한 참조
    /// </summary>
    public Transform playerPosition;

    /// <summary>
    /// 카메라가 플레이어를 따라가는 속도
    /// </summary>
    public float followSpeed;

    /// <summary>
    /// 카메라가 따라갈 목표 위치 계산
    /// </summary>
    private Vector3 TargetPosition
        => playerPosition.position + DEFAULT_LOCAL_POSITION;


    private void Start()
    {
        transform.position = playerPosition.position + DEFAULT_LOCAL_POSITION;

        // 카메라의 초기 회전 설정
        transform.rotation = Quaternion.Euler(DEFAULT_LOCAL_ROTATION);
    }
    void Update()
    {
        // 부드럽게 따라가기 위해 Lerp를 사용하여 카메라를 목표 위치로 이동
        transform.position = Vector3.Lerp(transform.position, TargetPosition, followSpeed);
    }
}
