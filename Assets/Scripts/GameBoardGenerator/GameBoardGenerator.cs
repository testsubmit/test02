using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 지도 생성에 사용될 수 있는 GameBoardGenerator 클래스
/// </summary>
public class GameBoardGenerator : MonoBehaviour
{
    /// <summary>
    /// 도로로 사용할 프리팹 배열
    /// </summary>
    public GameObject[] roadPrefabs;

    /// <summary>
    /// 현재 프리팹 인덱스 및 증가 메서드
    /// </summary>
    private int PrefabIndex = 0;
    private void IncreaseIndex()
    {
        // 인덱스 증가 및 배열 길이로 나눈 나머지로 순환시킴
        PrefabIndex++;
        PrefabIndex %= roadPrefabs.Length;
    }

    /// <summary>
    /// 도로 생성 메서드
    /// </summary>
    /// <returns>해당 프리팹을 인스턴스화하여 반환</returns>
    public GameObject CreateRoad()
    {
        // 현재 프리팹 인덱스 저장 후 인덱스 증가
        int index = PrefabIndex;
        IncreaseIndex();

        // 무작위로 선택한 프리팹을 인스턴스화하여 반환
        return Instantiate(roadPrefabs[Random.Range(0, roadPrefabs.Length)]);
    }

}