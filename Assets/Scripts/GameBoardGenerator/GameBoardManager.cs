using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// 맵을 관리하고 이동 대상 오브젝트로부터 상속된 클래스
/// </summary>
public class GameBoardManager : MoveToTargetObject
{
    /// <summary>
    /// 초기 도로 생성 수
    /// </summary>
    public const int InitiRoadCount = 25;

    /// <summary>
    /// 맵 생성기에 대한 참조
    /// </summary>
    public GameBoardGenerator gameBoardCreater;

    /// <summary>
    /// 플레이어 입력 관리자
    /// </summary>
    public PlayerInputManager playerInputManager;

    /// <summary>
    /// 마지막 도로의 최소 Z 위치
    /// </summary>
    public float lastRoadMinZPos = 0;

    /// <summary>
    /// 생성된 라인 그룹을 저장하는 리스트
    /// </summary>
    private List<LineObjectGroup> LineGroupsList = new List<LineObjectGroup>();

    /// <summary>
    /// 도로의 z축 크기
    /// </summary>
    private const float ZLineSize = 2;

    /// <summary>
    /// 뒤로 이동할때 사망하는 칸의 수
    /// </summary>
    private int BackEndCount = 3;

    private void Start()
    {
        //플레이어 인풋매니저에서 수직 방향의 이동이 들어오면 
        playerInputManager.vertical += MoveVertical;

        MapGenerate();  
    }

    /// <summary>
    /// 맵을 초기화하고 초기 도로를 생성
    /// </summary>
    private void MapGenerate()
    {
        for (int i = 0; i < InitiRoadCount; ++i)
        {
            // 도로를 생성합니다.
            GameObject roadInstance = gameBoardCreater.CreateRoad();

            // 해당 위치에 도로를 위치
            roadInstance.transform.position = new Vector3(0, 0, i * ZLineSize);
            roadInstance.transform.parent = transform;

            LineGroupsList.Add(roadInstance.GetComponent<LineObjectGroup>());
        }

        // 마지막 도로의 Z 위치를 기록
        lastRoadMinZPos = LineGroupsList[LineGroupsList.Count - 1].transform.position.z;

        SetLastRoadToInputManager();
    }

    /// <summary>
    /// 플레이어 입력 관리자에게 마지막 도로를 이동 대상으로 설정
    /// </summary>
    private void SetLastRoadToInputManager()
    {
        //라인 그룹 리스트의 수가 0보다 작거나 같다면 반환
        if (LineGroupsList.Count <= 0) return;

        playerInputManager.verticalMoveToTarget = LineGroupsList[LineGroupsList.Count - 1];
    }

    /// <summary>
    /// 새로운 도로를 생성
    /// </summary>
    private void SetNewRoad()
    {
        // 도로를 생성
        GameObject Line = gameBoardCreater.CreateRoad();

        // 새 도로의 위치를 설정. 이전 도로의 위치에서 Z축 방향으로 일정 거리 떨어져 있도록 함
        Line.transform.position = LineGroupsList[LineGroupsList.Count - 1].transform.position + new Vector3(0, 0, 2);

        // 새 도로를 이 객체의 자식으로 설정
        Line.transform.parent = transform;

        // 새로 생성한 도로를 도로 그룹 리스트에 추가
        LineObjectGroup roadGroup = Line.GetComponent<LineObjectGroup>();
        LineGroupsList.Add(roadGroup);

        // 플레이어 입력 관리자에게 마지막 도로를 이동 대상으로 설정
        SetLastRoadToInputManager();
    }

    /// <summary>
    /// 가장 오래된 도로를 삭제
    /// </summary>
    private void DeleteLastRoad()
    {
        GameObject lastRoad = LineGroupsList[0].gameObject;

        LineGroupsList.RemoveAt(0);

        Destroy(lastRoad);
    }

    /// <summary>
    /// 세로 이동을 처리
    /// </summary>
    /// <param name="dir">이동 방향</param>
    private void MoveVertical(int dir)
    {
        // 이미 이동 중인 경우 함수를 종료
        if (base.isMove) return;

        // 이동 방향을 두 배로 증가
        dir *= -2;

        // 이동 방향이 양수인 경우, 후진 카운터를 감소
        if (dir > 0)
        {
            --BackEndCount;
        }

        // 이동 방향이 음수인 경우, 후진 카운터를 초기화
        else
        {
            BackEndCount = 3;
        }

        // 도로 그룹을 이동
        MoveGroups(new Vector3(0, 0, dir));

        // 후진 카운터가 0 이하인 경우, 게임 오버를 호출
        if (BackEndCount <= 0)
            GameManager.instance.GameOver();
    }

    /// <summary>
    /// 마지막 도로의 위치를 확인
    /// </summary>
    /// <returns>마지막 도로가 최소 위치에 있는 경우 true, 그렇지 않으면 false를 반환</returns>
    private bool CheckLastRoadPosition()
    {
        // 리스트가 비어있는 경우 false를 반환
        if (LineGroupsList.Count <= 0) return false;

        // 마지막 도로 세그먼트를 가져옴
        GameObject lastRoad = LineGroupsList[LineGroupsList.Count - 1].gameObject;

        // 마지막 도로 세그먼트의 Z 위치가 최소 Z 위치보다 작거나 같으면 true를 반환
        if (lastRoad.transform.position.z <= lastRoadMinZPos)
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// 모든 도로 그룹을 이동
    /// </summary>
    /// <param name="dir">이동 방향입니다.</param>
    private void MoveGroups(Vector3 dir)
    {
        // 후진하고 마지막 도로가 최소 위치에 있는 경우, 새로운 도로를 생성하고 가장 오래된 도로를 삭제
        if (dir.z < 0 && CheckLastRoadPosition())
        {
            SetNewRoad();
            DeleteLastRoad();
        }

        // 모든 도로 그룹을 이동
        foreach (LineObjectGroup group in LineGroupsList)
        {
            group.SetMoveDir(dir);
        }
    }
}

