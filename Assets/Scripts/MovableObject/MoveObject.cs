using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 이동 가능한 객체를 담고 있는 네임스페이스
/// </summary>
namespace MoveObejct
{
    /// <summary>
    /// 이동 가능 객체를 나타내는 추상 클래스
    /// </summary>
    public abstract class MoveObject : MonoBehaviour
    {
        /// <summary>
        /// 이동 속도를 결정하는 변수
        /// </summary>
        public float moveSpeed;

        /// <summary>
        /// 이동 시작 위치를 저장
        /// </summary>
        private Transform CreatePosition;

        /// <summary>
        /// 이동할 목표 위치를 저장
        /// </summary>
        private Transform TargetPosition;

        /// <summary>
        /// 이동 방향을 정규화한 벡터를 반환
        /// </summary>
        private Vector3 MoveDirection 
            => (TargetPosition.position - transform.position).normalized;

        /// <summary>
        /// 목표 지점에 도착했는지 여부를 반환
        /// /// </summary>
        private bool Arrive 
            => Vector3.Distance(CreatePosition.position, TargetPosition.position) <= Vector3.Distance(CreatePosition.position, transform.position);



        /// <summary>
        /// 객체를 초기화
        /// </summary>
        /// <param name="createPos">이동 시작 위치</param>
        /// <param name="targetPos">이동할 목표 위치</param>
        public void Initialized(Transform createPos, Transform targetPos)
        {
            this.CreatePosition = createPos;
            this.TargetPosition = targetPos;
        }
        private void FixedUpdate()
        {
            // 목표 지점에 도착했다면 객체를 제거
            if (Arrive)
            {
                Destroy(gameObject);
            }

            // 그렇지 않으면 이동
            else
            {
                transform.position = transform.position + MoveDirection * moveSpeed * Time.fixedDeltaTime;
            }
        }
    }
}

