using UnityEngine;


namespace MoveObejct
{
    /// <summary>
    /// 자동차의 이동을 제어
    /// </summary>
    public class CarMovement : MoveObject
    {
        /// <summary>
        /// 충돌이 발생할때 호출
        /// </summary>
        /// <param name="collision">충돌 정보</param>
        private void OnCollisionEnter(Collision collision)
        {
            // 충돌한 오브젝트가 플레이어 캐릭터인 경우, 플레이어의 게임 오버를 호출
            collision.transform.GetComponent<PlayerCharacter>().PlayerGameOver();
        }
    }
}